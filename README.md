### Como rodar a aplicação ###
mvn spring-boot:run


# Minhas considerações #

- Eu decidi deixar o projeto o mais simples possível.

- Devido ao curto tempo que eu tinha, apenas dois testes foram criados, unitário básico e integração. Sei que existem outros tipos de testes e que a cobertura está pouca, mas acho que isso mostra que sei criá-los.


#Rest API

##API de personagem

- Esse serviço REST faz duas chamadas a Maven API. 
- A primeira chamada é a busca do personagem pelo nome, a segunda busca as revistas do personagem e encontra a revista mais cara.
- Os dados consolidados são colocados no cache.
- O nome a ser buscado deve trazer apenas 1 personagem, caso traga zero ou mais de dois, uma execeção é lançada.
 

### Exemplo
### http://localhost:8080/character?characterName=Wolverine
{
"characterId": 1009718,
"name": "Wolverine",
"mostExpensivePrintPrice": 24.99,
"title": "Wolverine: Season One (Hardcover)"
}


##API para buscar dados consolidados
- Com os dados já consolidados na mémoria após uma consulta na API de personagemn é possível recuperá-lo através dessa API passando o id do personagem.
- Essa API também permite a inserção, alteração e remoção de dados diretamente da memória.

- Existem as seguintes metodos:

- GET: /comicDetail/{id}
- POST: /comicDetail
- PUT: /comicDetail
- DELETE: /comicDetaill/{id}

### Exemplo do uso do GET
http://localhost:8080/comicDetail/1009718
{
"characterId": 1009718,
"name": "Wolverine",
"mostExpensivePrintPrice": 24.99,
"title": "Wolverine: Season One (Hardcover)"
}
