package br.com.treinamento.dojo.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.model.ComicDetail;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class, loader=SpringApplicationContextLoader.class)
public class ComicDetailServiceTest {
	
	@Autowired
	private CharactersService charactersService;	
	
	@Test
	public void testGettingWolverine(){
		
		ComicDetail detail = charactersService.getMostExpensiveComic("Wolverine");
		
		assertNotNull(detail);			
		
	}
	
	
	

}
