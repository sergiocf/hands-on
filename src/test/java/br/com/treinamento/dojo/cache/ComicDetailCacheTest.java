package br.com.treinamento.dojo.cache;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.treinamento.dojo.model.ComicDetail;

public class ComicDetailCacheTest {
	
	@Test
	public void testAddingAndGettingValue() {
		ComicDetail detail = new ComicDetail();
		detail.setCharacterId(1);
		detail.setMostExpensivePrintPrice(99f);
		detail.setName("Test");
		detail.setTitle("Test Title");
		
		ComicDetailCache.getInstance().add(detail);
				
		assertNotNull(ComicDetailCache.getInstance().get(1));
				
		assertTrue(detail.getName().equals(ComicDetailCache.getInstance().get(1).getName()));
	
		
	}

}
