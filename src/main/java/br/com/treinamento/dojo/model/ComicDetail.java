package br.com.treinamento.dojo.model;

public class ComicDetail {
	
	private Integer characterId;
	
	private String name;
	
	private float mostExpensivePrintPrice;
	
	private String title;

	public Integer getCharacterId() {
		return characterId;
	}

	public void setCharacterId(Integer characterId) {
		this.characterId = characterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getMostExpensivePrintPrice() {
		return mostExpensivePrintPrice;
	}

	public void setMostExpensivePrintPrice(float mostExpensivePrintPrice) {
		this.mostExpensivePrintPrice = mostExpensivePrintPrice;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "ComicDetail [characterId=" + characterId + ", name=" + name
				+ ", mostExpensivePrintPrice=" + mostExpensivePrintPrice
				+ ", title=" + title + "]";
	}	

}
