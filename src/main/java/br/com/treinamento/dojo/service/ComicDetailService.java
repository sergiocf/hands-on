package br.com.treinamento.dojo.service;

import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.cache.ComicDetailCache;
import br.com.treinamento.dojo.model.ComicDetail;

@Service
public class ComicDetailService {
	
	public ComicDetail getComicDetail(Integer id) {
		return ComicDetailCache.getInstance().get(id);
	}
	
	public void insertOrUpdate(ComicDetail comicDetail) {
		ComicDetailCache.getInstance().add(comicDetail);					

	}
	
	public void delete(Integer characterId) {
		ComicDetailCache.getInstance().remove(characterId);					

	}

}
