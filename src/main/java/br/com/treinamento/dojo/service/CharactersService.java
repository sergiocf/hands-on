package br.com.treinamento.dojo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.cache.ComicDetailCache;
import br.com.treinamento.dojo.marvel.api.ApiReturn;
import br.com.treinamento.dojo.marvel.api.Comic;
import br.com.treinamento.dojo.marvel.api.MarvelCharacter;
import br.com.treinamento.dojo.marvel.api.MavenApiHelper;
import br.com.treinamento.dojo.model.ComicDetail;


@Service
public class CharactersService {
	
	@Autowired
	private MavenApiHelper helper;

    public ComicDetail getMostExpensiveComic(String characterName) {
    	
    	//Consulta personagem usando a API da Marvel
    	ApiReturn<MarvelCharacter> marvelCharacter =  helper.getCharacter(characterName);
    	
    	if(marvelCharacter.getData().getTotal() != 1 ) {
 		   throw new RuntimeException("Zero or more than one character found! It must to be only one!");
    		
    	} 	 	
    	
    	ComicDetail comicDetail = new ComicDetail();
    	comicDetail.setName(marvelCharacter.getData().getResults().get(0).getName());
    	comicDetail.setCharacterId(marvelCharacter.getData().getResults().get(0).getId());
    	
    	
    	//Consulta todos os comics do personagem pesquisado
    	ApiReturn<Comic> comics =  helper.getComics(comicDetail.getCharacterId());
    	
    	//Encontra o comic mais caro
    	Comic mostExpensiveComic = getMostExpensivePrintPrice(comics.getData().getResults());    	
    	comicDetail.setMostExpensivePrintPrice(mostExpensiveComic.getPrintPrice());
    	comicDetail.setTitle(mostExpensiveComic.getTitle());  	
    	    	
    	//Salva no cache os dados já consolidados do personagem
    	ComicDetailCache.getInstance().add(comicDetail);
    	
    	return comicDetail;
    }
    
    private Comic getMostExpensivePrintPrice(List<Comic> comics) {
    	
    	Comic result = null;
    	
    	for(Comic comic: comics) {
    		
    		if(result == null || comic.getPrintPrice() > result.getPrintPrice()){
    			result = comic;
    		}    		
    		
    	}
    	
    	return result;    	
    }

	
}



