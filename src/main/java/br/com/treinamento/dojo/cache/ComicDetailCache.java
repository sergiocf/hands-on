package br.com.treinamento.dojo.cache;
import java.util.HashMap;
import java.util.Map;

import br.com.treinamento.dojo.model.ComicDetail;



/**
 * Responsável por salvar em memória as consultas já consolidas feitas usando a API da Marvel
 * @author sfonseca
 *
 */
public class ComicDetailCache {
	
	private final static ComicDetailCache instance = new ComicDetailCache();
	public Map<Integer, ComicDetail> cache = new HashMap<>();
	
	
	private ComicDetailCache() {		
	
	}	
	
	public static ComicDetailCache getInstance() {		
		return instance;		
	}
	
	public void add(ComicDetail comicDetail) {		
		cache.put(comicDetail.getCharacterId(), comicDetail);
		
	}
	
	public ComicDetail get(Integer characterId) {
		return cache.get(characterId);
	}
	
	public ComicDetail remove(Integer characterId) {
		return cache.remove(characterId);
	}
	

}
