package br.com.treinamento.dojo.marvel.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ComicPrice {

    private String type;

    private float price;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    
    public boolean isPrintPriceType() {
    	return "printPrice".equals(type) ;
    }

	@Override
	public String toString() {
		return "ComicPrice [type=" + type + ", price=" + price + "]";
	}

}
