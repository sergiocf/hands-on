package br.com.treinamento.dojo.marvel.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiReturn<E> {

    private int code;

    private String status;

    private String copyright;

    private String attributionText;

    private String attributionHtml;

    private String eTag;

    private DataContainer<E> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getAttributionText() {
        return attributionText;
    }

    public void setAttributionText(String attributionText) {
        this.attributionText = attributionText;
    }

    public String getAttributionHtml() {
        return attributionHtml;
    }

    public void setAttributionHtml(String attributionHtml) {
        this.attributionHtml = attributionHtml;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public DataContainer<E> getData() {
        return data;
    }

    public void setData(DataContainer<E> data) {
        this.data = data;
    }

	@Override
	public String toString() {
		return "ApiReturn [code=" + code + ", status=" + status
				+ ", copyright=" + copyright + ", attributionText="
				+ attributionText + ", attributionHtml=" + attributionHtml
				+ ", eTag=" + eTag + ", data=" + data + "]";
	}
    
    


}