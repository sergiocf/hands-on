package br.com.treinamento.dojo.marvel.api;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class DataContainer<E> {

    private int offset;

    private int limit;

    private int total;

    private int call;

    private List<E> results;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCall() {
        return call;
    }

    public void setCall(int call) {
        this.call = call;
    }

    public List<E> getResults() {
        return results;
    }

    public void setResults(List<E> results) {
        this.results = results;
    }

	@Override
	public String toString() {
		return "DataContainer [offset=" + offset + ", limit=" + limit
				+ ", total=" + total + ", call=" + call + ", results="
				+ results + "]";
	}  


}
