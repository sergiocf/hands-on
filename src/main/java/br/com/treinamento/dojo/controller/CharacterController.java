package br.com.treinamento.dojo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.model.ComicDetail;
import br.com.treinamento.dojo.service.CharactersService;

@RestController
public class CharacterController {
	
	@Autowired
	private CharactersService charactersService;

	@RequestMapping(value = "/character", method = RequestMethod.GET)
	public ResponseEntity<ComicDetail> comicDetail(@RequestParam String characterName) {		
		
		ComicDetail  result = charactersService.getMostExpensiveComic(characterName);

		return new ResponseEntity<ComicDetail>(result, HttpStatus.OK);
	}

}
