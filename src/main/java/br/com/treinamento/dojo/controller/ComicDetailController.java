package br.com.treinamento.dojo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.model.ComicDetail;
import br.com.treinamento.dojo.service.ComicDetailService;

@RestController
public class ComicDetailController {

	@Autowired
	private ComicDetailService service;

	@RequestMapping(value = "/comicDetail/{id}", method = RequestMethod.GET)
	public ResponseEntity<ComicDetail> get(@PathVariable Integer id) {

		ComicDetail result = service.getComicDetail(id);

		if (result == null) {
			throw new RuntimeException("Id not flound!");
		}

		return new ResponseEntity<ComicDetail>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/comicDetail", method = RequestMethod.PUT)
	public ResponseEntity<ComicDetail> update(ComicDetail comicDetail) {
		service.insertOrUpdate(comicDetail);

		return new ResponseEntity<ComicDetail>(comicDetail, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/comicDetail", method = RequestMethod.POST)
	public ResponseEntity<ComicDetail> insert(ComicDetail comicDetail) {
		service.insertOrUpdate(comicDetail);

		return new ResponseEntity<ComicDetail>(comicDetail, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/comicDetaill/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable Integer id) {
		service.delete(id);

		return new ResponseEntity<String>("id deleted!", HttpStatus.OK);
	}

}
